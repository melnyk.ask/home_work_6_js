//#6!!
//1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Екранування - моливість перетворити сцеціальний символ на звичайний. Це робиться за допомогою
// символу "\", чкий розташовується перед спецсимволом, який треба екранувати.
// Наприклад, при пошуку символу "$" в рядку:
// text = "213$ 234 bn  fdgfg";
// console.log(text.search(/\$/));
// отримаємо відповідь 3 (індекс символу "$").

//2.Які засоби оголошення функцій ви знаєте?
// Є два способи оголошення функцій.
// Перший: function declaration.
// function one() {
//     return "one";
// }
// Другий: function expression.
// let two = function () {
//     return "two";
// };
// В першому випадку ми можемо використовувати функцію до того, як оголосили її.
// В другому випадку функція створюється тільки тоді, коли виконання програми доходить
// до рядку з функцією.

//3. Що таке hoisting, як він працює для змінних та функцій?
// Hoisting для функцій дає можливість викликати функцію до оголошення (у випадку function declaration).
// Якщо змінну оголошувати за допомогою let, const, потрібно оголошувати до ініціалізації. І вже
// після цього використовувати. Якщо використовувати до - отримаємо помилку.
// А от у випадку оголошення змінної за допомогою var - можливо спочатку присвоїти значення, а
// потім оголосити змінну.


// 1. 
function createNewUser() {

    let firstName = prompt("Enter first name");
    let lastName = prompt("Enter last name");
    let birth = prompt("Enter data dd.mm.yyyy");
    let newUser = {
        "firstName" : firstName,
        "lastName": lastName,
        "birthday": birth,
        getLogin() { 
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();            
        },        
        getAge() {
            let day = +birth.slice(0, 2);
            let month = +birth.slice(3, 5);
            let year = +birth.slice(6, 10);
            let a = new Date();
            let b = new Date();
            b.setFullYear(year, month - 1, day);
            let diff = a - b;
            let user_age = new Date(diff);
            
            return user_age.getFullYear() - 1970;
        },
        getPassword() { 
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10); 
        },
        setFirstName(str) { 
            Object.defineProperty(this, "firstName",
                {
                    value : str
                }
            );
        },
        setLastName(str) { 
            Object.defineProperty(this, "lastName",
                {
                    value : str
                }
            );
        }
    };
    Object.defineProperty(newUser, "firstName",
        {
            writable: false,
            configurable: true
        }    
    );
    
    Object.defineProperty(newUser, "lastName",
        {
            writable: false,
            configurable: true
        }    
    );  
   
    return newUser;
}

let testUser = createNewUser();

console.log(testUser);
console.log(testUser.getAge());
console.log(testUser.getPassword());



